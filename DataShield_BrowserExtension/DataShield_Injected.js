(function() {
	window.DataShield = window.DataShield || {
		debug:1,
		init:function() {
			if(this.initted)	return this;
			this.initted=1;
			var _this=this;
			// Set default timeout for 10 secs for XHR when open() is called.
			this.XMLHttpRequest_Open = window.XMLHttpRequest.prototype.open;
			window.XMLHttpRequest.prototype.open = function() {
				var x = _this.XMLHttpRequest_Open.apply(this, arguments);
				x.timeout = 10000;
				return x;
			}
			debug && console.log("DataShield: Set default timeout for 10 secs for XHR when open() is called.");
			return this;
		},
		injectCSS:function() {
			if(this.cssInjected)	return this;
			this.cssInjected=1;
			
		}
	}
	return window.DataShield.init();
})();