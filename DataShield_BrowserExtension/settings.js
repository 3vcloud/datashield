var webRequest = chrome.webRequest || browser.webRequest;
var storage = chrome.storage || browser.storage;
var runtime = chrome.runtime || browser.runtime;
var browsertabs = chrome.tabs || browser.tabs;

var rowElements = {}
var domainRowElements = {}

var els = document.getElementsByClassName("row");
for(var i=0;i<els.length;i++) {
	var maps_to = els[i].getAttribute('maps_to');
	if(!maps_to)
		continue;
	if(/domainRow/.test(els[i].className))
		domainRowElements[maps_to] = els[i];
	else
		rowElements[maps_to] = els[i];
}
var dataShieldSettingsContainer = document.getElementById('dataShieldSettingsContainer');
var useGlobalSettings = document.getElementById('useGlobalSettings');
var disableDataShield = document.getElementById('disableDataShield');
var domainName = document.getElementById('domainName');
var domainLevelSettingsInner = document.getElementById('domainLevelSettingsInner');
var blockedList = document.getElementById('blockedUrlsList');

var current_domain = false;

for(let i in rowElements) {
	if(!rowElements[i])
		continue;
	rowElements[i].addEventListener('click', function() {
		toggleMediaType(i,false);
	});
}
for(let i in domainRowElements) {
	if(!domainRowElements[i])
		continue;
	domainRowElements[i].addEventListener('click', function() {
		if(useGlobalSettings.checked || !current_domain)
			return false;
		toggleMediaType(i,current_domain);
	});
}
useGlobalSettings.addEventListener('click', function() {
	toggleDomainLevelSettings();
	return false;
});
disableDataShield.addEventListener('click', function() {
	toggleMediaType('disabled',current_domain);
	return false;
});
runtime.sendMessage({msg: "getBlockedMediaTypes"}, redrawClasses);
getCurrentTab(function(tab) {
	if(!tab) return;
	runtime.sendMessage({msg: "getBlockedSoFar",tab:tab}, redrawBlockedUrls);
});
function reloadImages() {
	
}
function toggleDomainLevelSettings() {
	runtime.sendMessage({msg: "toggleDomainLevelSettings",domain:current_domain}, redrawClasses);
}
function toggleMediaType(mediaType,domain) {
	runtime.sendMessage({msg: "toggleMediaType",mediaType:mediaType,domain:domain}, redrawClasses);
	//reloadCurrentTab();
}
function redrawBlockedUrls(response) {
	//console.log("Response2",response);
	var html = '';
	if(!response.length)
		return blockedList.innerHTML = html, true;
	var d;
	for(var i=0;i<response.length;i++) {
		d = new Date(response[i].date);
		html += '<p class=reason title="'+response[i].reason+'">['+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()+']['+response[i].type.toUpperCase()+'] '+response[i].url+'</p>';
	}
	return blockedList.innerHTML = html, true;
}
function redrawClasses(response) {
	console.log("Response1",response);
	getCurrentTab(function(tab) {
		if(tab && tab.url)
			current_domain = domainFromURL(tab.url);
		
		// Set Global vars
		var domainLevelSettings = {};
		for(var i in response) {
			if(typeof response[i] !== 'number')
				continue;
			if(!rowElements[i])
				continue;
			domainLevelSettings[i] = response[i];
			switch(response[i]) {
				case 1:		
					rowElements[i].classList.remove('enabled');
					rowElements[i].classList.remove('nofeature');
				break;
				case 0:
					rowElements[i].classList.add('enabled');
					rowElements[i].classList.remove('nofeature');
				break;
				default:
					rowElements[i].classList.add('nofeature');
				break;
			}
		}
		if(!current_domain)
			return document.getElementById('domainLevelSettings').style.display='none';
		domainName.innerHTML=current_domain;
		// Set domain level vars.
		if(response.byDomain[current_domain]) {
			for(var i in response.byDomain[current_domain]) {
				domainLevelSettings[i] = response.byDomain[current_domain][i];
			}
			dataShieldSettingsContainer.classList.remove('notDomainLevel');
			useGlobalSettings.checked = false;
		} else {
			dataShieldSettingsContainer.classList.add('notDomainLevel');
			useGlobalSettings.checked = true;
		}
		
		if(domainLevelSettings.disabled) {
			dataShieldSettingsContainer.classList.add('dataShieldDisabled');
			disableDataShield.checked = true;
		} else {
			dataShieldSettingsContainer.classList.remove('dataShieldDisabled');
			disableDataShield.checked = false;
		}
		for(var i in domainLevelSettings) {
			if(typeof domainLevelSettings[i] !== 'number')
				continue;
			if(!domainRowElements[i])
				continue;
			switch(domainLevelSettings[i]) {
				case 1:		
					domainRowElements[i].classList.remove('enabled');
					domainRowElements[i].classList.remove('nofeature');
				break;
				case 0:
					domainRowElements[i].classList.add('enabled');
					domainRowElements[i].classList.remove('nofeature');
				break;
				default:
					domainRowElements[i].classList.add('nofeature');
				break;
			}
		}
		
	});
}
function domainFromURL(url) {
	var matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
	var domain = matches && matches[1];  // domain will be null if no match is found
	if(!domain)
		return false;
	return domain;
}
function getCurrentTab(cb) {
	try {
		browsertabs.query({active: true, lastFocusedWindow: true}, function(tabs) {
			cb.apply(this,[tabs[0]]);
		});
	} catch(e) {
		return false;
	}
}