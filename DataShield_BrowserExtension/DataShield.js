// ==UserScript==
// @name         DataShield Media Blocker
// @description  Prevents download of third party media files (Video, Images, CSS, JS, Fonts) reducing data usage and reducing load times.
// @version 1.1.1
// @icon icon_128.png
// @include      *
// @run-at document-start
// ==/UserScript==

(function() {
	'use strict'
	/*	VARIABLES	*/
	var browser = browser || chrome;
	var whitelist = [
		/^(?!http)/,					// Any non-http URLs
		/^https?:\/\/[^\/]+\.gov\./,	// Govt websites e.g. *.gov.uk
		/^https?:\/\/local/				// Local URLs e.g. local.
	]
	var defaultBlockedMediaTypes = {
		images:1,
		videos:1,
		fonts:1,
		css:0,
		js:0,
		turbo:2,
		nocache:0,
		marketing:1,
		social_media:1,
		google_maps:1,
		byDomain:{}
	}
	var blockedMediaTypes=false;
	var tabsById = {};
	var marketing_keywords = [
		'adhub','\.cx','log','event','counter','fls-na\.amazon\.com','ClientSideMetrics','hotjar','scorecardresearch',
		'sharethrough','metric','promot(e|ion)','media\.net','visualweb','research','pageview','loudloss',
		'eyeota','viglink','quant(serve|cast)','taboo\.?la','carambo\.?la','advert','adsystem','adservice',
		'googlesyn','ads','activity','chart','survey','googlead','doubleclick','bing','collect(or)?',
		'track','criteo','bluekai','contently','analytics','market(ing)?','salesforce','googletag'
	]
	var marketing_url_regex = new RegExp('\\b('+marketing_keywords.join('|')+')\\b','i'); // OLD REGEXP: new RegExp('[^a-zA-Z0-9]('+marketing_keywords.join('|')+')(?:[^a-zA-Z0-9]|$)','i');
	var marketing_domain_regex = marketing_url_regex; // OLD REGEXP: new RegExp('[^\.]*('+marketing_keywords.join('|')+').*','i');

	var video_url_regex = /^https?[^?#]+[.](mpe?g|wma|avi|mp4|webm|aac|mp3|wav|ogg|ogv|ts)([?#]|$)/i
	var css_url_regex = /^https?[^?#]+[.](s?css|less)([?#]|$)/i
	var js_url_regex = /^https?[^?#]+[.](js|javascript|json)([?#]|$)/i
	var blocks_by_tab = {};
	/*	FUNCTIONS	*/
	function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}
	function isSubdomain(domain1,domain2) {
		domain1 = domain1.replace(/www\./,'');
		domain2 = domain2.replace(/www\./,'');
		return (new RegExp('(?:^|[.])'+domain1+'$')).test(domain2) || (new RegExp('(?:^|[.])'+domain2+'$')).test(domain1);
	}
	function isImage(request) {
		if(request.type && /^(image|jpe?g|png|gif|svg|webp|img|ico)/i.test(request.type)) 
			return "Request type is image";
		if(request.url && /^https?[^?#]+[.](jpe?g|png|gif|svg|webp|img|ico)([?#]|$)/i.test(request.url))	
			return "Request URL matches image regex";
		if(request.responseHeaders) {
			for(var i in request.responseHeaders) {
				if(/^content-type$/i.test(request.responseHeaders[i].name))
					return /^image/i.test(request.responseHeaders[i].value) ? "Content-Type header in response includes image" : false;
			}
		}
		if(request.requestHeaders) {
			for(var i in request.requestHeaders) {
				if(/^accept$/i.test(request.requestHeaders[i].name))
					return /^image/i.test(request.requestHeaders[i].value) ? "Accept header in request includes image" : false;
			}
		}
		return false;
	}
	function isVideo(request) {
		if(request.type && /^(video|media)/i.test(request.type))	
			return "Request type is video";
		if(request.url && video_url_regex.test(request.url))		
			return "Request URL matches video regex";
		if(request.url && /[^a-zA-Z0-9](video|audio)[^a-zA-Z0-9]/i.test(request.url))	
			return "Request URL contains the word video or audio";
		var referrer_domain = domainFromRequest(request);
		var request_domain =  domainFromURL(request.url);
		if(/video|audio|player/i.test(request_domain) && !isSubdomain(referrer_domain,request_domain))
			return "Request domain matches video regex";
		if(request.responseHeaders) {
			for(var i in request.responseHeaders) {
				if(/^content-type$/i.test(request.responseHeaders[i].name))
					return /^video/i.test(request.responseHeaders[i].value) ? "Content-Type header in response includes video" : false;
			}
		}
		if(request.requestHeaders) {
			for(var i in request.requestHeaders) {
				if(/^accept$/i.test(request.requestHeaders[i].name))
					return /^(audio|video)/i.test(request.requestHeaders[i].value) ? "Accept header in request includes image" : false;
			}
		}
		return false;
	}
	function getURL(url) {
		if(url.url)	return url.url;
		return url ? url : false;
	}
	function isWhitelisted(url) {
		url = getURL(url);
		if(!url)	return false;
		for(var i in whitelist) {
			if(whitelist[i].test(url))
				return console.log("WL",url),1;
		}
		return 0;
	}
	function isCSS(request) {
		if(request.type && /style|\/css/i.test(request.type))
			return "Request type is style or CSS";
		if(request.url && css_url_regex.test(request.url))		
			return "Request URL matches css regex";
		if(request.responseHeaders) {
			for(var i in request.responseHeaders) {
				if(/^content-type$/i.test(request.responseHeaders[i].name))
					return /style|\/css/i.test(request.responseHeaders[i].value) ? "Content-Type header in response includes style or css" : false;
			}
		}
		if(request.requestHeaders) {
			for(var i in request.requestHeaders) {
				if(/^accept$/i.test(request.requestHeaders[i].name))
					return /^text\/css/i.test(request.requestHeaders[i].value) ? "Accept header in request includes style or css" : false;
			}
		}
		return false;
	}
	function isJS(request) {
		if(request.type && /script/i.test(request.type))
			return "Request type is script";
		if(request.url && js_url_regex.test(request.url))		
			return "Request URL matches JS regex";
		if(request.responseHeaders) {
			for(var i in request.responseHeaders) {
				if(/^content-type$/i.test(request.responseHeaders[i].name))
					return /script/i.test(request.responseHeaders[i].value) ? "Content-Type header in response includes script" : false;
			}
		}
		return false;
	}
	function isHTML(request) {
		var r = /^(document|[a-z]+_frame|(text|application)\/html)/i;
		if(request.type)
			return r.test(request.type);
		if(request.responseHeaders) {
			for(var i in request.responseHeaders) {
				if(/^content-type$/i.test(request.responseHeaders[i].name))
					return r.test(request.responseHeaders[i].value);
			}
		}
		return false;
	}
	function isFont(request) {
		if(request.type && /font/i.test(request.type))	
			return "Browser request type is font";
		if(request.url && /[^a-zA-Z0-9](woff2?|ttf|eot|otf|(web)?font[s]?)([^a-zA-Z0-9]|$)/i.test(request.url))		
			return "URL matches font check";
		var referrer_domain = domainFromRequest(request) || '';
		var request_domain =  domainFromURL(request.url);
		var domain_regex = /[^\/.]*(typekit|fonts?).*/i;
		var match = domain_regex.exec(request_domain);
		if(match && !(new RegExp(match[0])).test(referrer_domain))
			return "Domain matches font regex checks";
		if(request.responseHeaders) {
			for(var i in request.responseHeaders) {
				if(/^content-type$/i.test(request.responseHeaders[i].name))
					return /font/i.test(request.responseHeaders[i].value) ? "Content-type response header is font" : false;
			}
		}
		return false;
	}
	function isSocialMedia(request) {
		// Is the file requested related to social media?
		var referrer_domain = domainFromRequest(request) || '';
		var request_domain =  domainFromURL(request.url);
		var domain_regex = /[^\/.]*(facebook|twitter|instagram|linkedin|addthis|youtube).*/i;
		var match = domain_regex.exec(request_domain);
		if(match && !isSubdomain(referrer_domain,match[0]))
			return "URL matches social media domain regex";	// If we're not on facebook, but the URL requested contains facebook, its blockable.
		return false;
	}
	function isMarketing(request) {
		var referrer_domain = domainFromRequest(request) || '';
		var request_domain =  domainFromURL(request.url);
		if(isSubdomain(referrer_domain,request_domain))
			return false; // Its ok if its the same domain.
		var regexp_domain = escapeRegExp(referrer_domain);
		var domain_regex = marketing_domain_regex;
		var url_regex = marketing_url_regex;
		var match;
		if(match = domain_regex.exec(request_domain))
			return "URL matches marketing domain regex: "+match[1];
		if(match = marketing_url_regex.exec(request.url))
			return "URL matches marketing regex: "+match[1];
		var regexp_domain_urlencoded = escapeRegExp(encodeURIComponent(referrer_domain));
		var referrer_in_query_test = new RegExp('[?].*'+regexp_domain_urlencoded);
		if(referrer_in_query_test.test(request.url))
			return "URL contains domain name in query parameter";
		return false;
	}
	function isGoogleMaps(request) {
		// Is the file requested related to google e.g. widgets etc?
		var gmaps_regexp = /^https?\:\/\/(maps\.google|google\.[^/]+\/maps)/;
		var referrer_url = tabURLFromRequest(request);
		if(gmaps_regexp.test(referrer_url))
			return false;	// Don't filter if we're currently on google maps.
		return gmaps_regexp.test(request.url) ? "URL matches google maps domain regex" : false;
	}
	function isMedia(request) {
		return isImage(request) || isVideo(request) || isCSS(request) || isJS(request) || isFont(request);
	}
	function tabUpdated(tabId,changedInfo,tab) {
		tabsById[tab.id] = tab;
		updateBadge(tab.id);
	}
	function tabCreated(tab) {
		tabsById[tab.id] = tab;
	}
	function getBlockedMediaForDomain(request) {
		var d = domainFromRequest(request);
		if(!d)	return blockedMediaTypes;
		if(!blockedMediaTypes.byDomain[d])
			return blockedMediaTypes;
		for(var i in blockedMediaTypes) {
			if(typeof blockedMediaTypes[i] === 'number' && typeof blockedMediaTypes.byDomain[d][i] === "undefined")
				blockedMediaTypes.byDomain[d][i] = blockedMediaTypes[i];
		}
		return blockedMediaTypes.byDomain[d];
	}
	function getCurrentTab(cb) {
		try {
			browser.tabs.query({active: true, lastFocusedWindow: true}, function(tabs) {
				cb.apply(this,[tabs[0]]);
			});
		} catch(e) {
			return false;
		}
	}
	function updateBadge(tabId) {
		var gotTabId = function(tabId) {
			if(!tabId)	return browser.browserAction.setBadgeText({text:null,tabId:tabId});
			var t = blocks_by_tab[tabId] || {'url':false,'blocked_urls':[]};
			return browser.browserAction.setBadgeText({text:t.blocked_urls.length+'',tabId:tabId});
		}
		if(tabId)
			return gotTabId(tabId);
		getCurrentTab(function(tab) {
			return gotTabId(tab ? tab.id : false)
			if(!tab) return 
			
		});
	}
	// Initially get tabs.
	chrome.tabs.query({},function(tabs) {
		for(var i in tabs) {
			tabsById[tabs[i].id] = tabs[i];
		}
	});
	chrome.tabs.onUpdated.addListener(tabUpdated);
	chrome.tabs.onCreated.addListener(tabCreated);
	
	var tBrowser = {};
	if(typeof browser !== 'undefined')		tBrowser = browser;
	if(typeof chrome !== 'undefined')		tBrowser = chrome;
	
	// Function to build up regexps based on preferences
	function init(cb) {
		load();
	}
	// If image, redirect to placeholder. Otherwise, cancel.
	// I found that some sites go on to do "other stuff" on a failed resource load. To mitigate, simply do a redirect.
	var pngUrl = chrome.runtime.getURL("BlockedByDataShield.png");
	var jsUrl = chrome.runtime.getURL("BlockedByDataShield.js");
	var cssUrl = chrome.runtime.getURL("BlockedByDataShield.css");
	function cancelOrRedirect(request,type='',reason='') {
		// Log this against the tab.
		var tab_url;
		if(request.url && request.tabId && (tab_url = tabURLFromRequest(request))) {
			if(!blocks_by_tab[request.tabId] || blocks_by_tab[request.tabId].url != tab_url)
				blocks_by_tab[request.tabId] = {'url':tab_url,'blocked_urls':[]};
			blocks_by_tab[request.tabId].blocked_urls.push({
				url:request.url,
				reason:reason,
				type:type,
				date:(new Date()).toString()
			});
			updateBadge(request.tabId);
		}
		if(request.requestHeaders || request.method != 'GET')
			return {cancel:true};	// Can't redirect from onBeforeSendHeaders
		if(isImage(request))
			return { redirectUrl: pngUrl};
		//if(isJS(request))
		//	return { redirectUrl: jsUrl}; 
		if(isCSS(request))
			return { redirectUrl: cssUrl};
		return {cancel:true};	// This should have the same effect as a 204 error?
	}
	function tabURLFromRequest(request) {
		if(request.tabId && tabsById[request.tabId])
			return tabsById[request.tabId].url;
		return false;
	}
	function isSSL(url) {
		return false;	// test
		if(url.tabId)	url = tabURLFromRequest(request_or_url);
		if(!url)	return false;
		return /^https/.test(url);
	}
	function domainFromRequest(request) {
		return domainFromURL(tabURLFromRequest(request));
	}
	function domainFromURL(url) {
		if(!url)	return false;
		var matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
		return matches && matches[1] ? matches[1] : false;  // domain will be null if no match is found
	}
	function cacheIfApplicable(request) {
		if(blockedMediaTypes.nocache)
			return {};	// Disabled
		if(!request.responseHeaders) {
			if(request.requestHeaders) {
				var newRequestHeaders = [];
				// Drop cache-control from request headers.
				for(var i in request.requestHeaders) {
					if(!/^cache\-control$/i.test(request.requestHeaders[i].name))
						newRequestHeaders.push(request.requestHeaders[i]);
				}
				return {requestHeaders:newRequestHeaders};
			}
			return {};
		}
		//console.log(request);
		if(!isHTML(request) && !isMedia(request))
			return {};	// Only applicable for document or media objects.
		var cacheString = 'max-age=315360000, public, immutable';
		var returnHeaders = [{name:'x-datashield-cached',value:'1'}];
		var handledCache=0;
		var existingCacheHeader=false;
		for(var i in request.responseHeaders) {
			if(/^cache\-control$/i.test(request.responseHeaders[i].name)) {
				existingCacheHeader = request.responseHeaders[i].value;
				continue;	// Log the current cache header for later.
			}
			returnHeaders.push(request.responseHeaders[i]);
		}
		if(isHTML(request)) {
			if(existingCacheHeader && false)	
				return console.log('Document already has a cache header',request.url,existingCacheHeader), {};		// Document already has a cache header.
			cacheString = 'max-age=86400, public';	// For documents, cache for 24 hours.
		}
		if(cacheString == existingCacheHeader)
			return {};	// No change
		returnHeaders.push({name:'Cache-control',value:cacheString});
		console.log("Cache overridden",request.url,existingCacheHeader,cacheString);
		return {responseHeaders:returnHeaders};
	}
	function fixURLsForCaching(request) {
		/*	Some resources are requested with a "version" query string but are intended to be cached.
			Chrome and Firefox often don't cache these pages, so try to remove the query string using a redirect.
		*/
		if(!/(ver|version|v)=/.test(request.url))
			return {};
		var url = request.url.replace(/\?[^#]/,function(query_string) {
			var r = /[?&](ver|version|v)=([^&]+|$)/i
			while(r.test(query_string)) {
				query_string = query_string.replace(r,function(str) {
					return str.charAt(0);
				});
			}
			//console.log(query_string);
			while(/&&|\?&/.test(query_string))
				query_string = query_string.replace(/&&/,'&').replace(/\?&/,'?');
			
			if(!/[a-zA-Z0-9]/.test(query_string))
				return '';
			return query_string;
		});
		console.log("Fixed URL for caching",request.url,url);
		return {redirectUrl:url};		
	}
	function filterOutJavascript(request) {
		if(!/^http/i.test(request.url))
			return {};
		var bm = getBlockedMediaForDomain(request);
		// Adds a filter to remove Javascript content from response body, IF request is for HTML.
		if(!bm.js || !isHTML(request))
			return console.log("JS enabled or not HTML content",request), {}; // JS enabled, or response is NOT HTML.
		let filter = tBrowser.webRequest.filterResponseData(request.requestId);
		let decoder = new TextDecoder("utf-8");
		let encoder = new TextEncoder();

		filter.ondata = function(event) {
			console.log("Filtering out <script> tags in response body",request.url);
			let str = decoder.decode(event.data, {stream: true});
			// Remove script tags from body
			var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gim;
			while (SCRIPT_REGEX.test(str))
				str = str.replace(SCRIPT_REGEX, '');
			filter.write(encoder.encode(str));
			filter.disconnect();
		}
		//console.log("Filter added for JS",request);
		return {};
	}
	function addCSPHeader(request) {
		if(!isHTML(request))			return {};
		if(!request.responseHeaders)	return {};
		var bm = getBlockedMediaForDomain(request);
		if(!bm.js)	return {};
		if(bm.disabled)	return {};
		var csp_header = false;
		var newHeaders = [];
		var default_csp_header = "script-src http: https:; ";
		for(var i in request.responseHeaders) {
			if(/content-security-policy|webkit-csp/i.test(request.responseHeaders[i].name)) {
				if(csp_header)
					continue;	// Already set header.
				csp_header = default_csp_header+request.responseHeaders[i].value.replace(/script-src[^;]+;?/g,'');
			} else {
				newHeaders.push(request.responseHeaders[i]);
			}
		}
		newHeaders.push({name:'Content-Security-Policy',value:csp_header || default_csp_header});
		console.log("Added Content-Security-Policy",csp_header || default_csp_header,request);
		return {responseHeaders:newHeaders};
	}
	function dropIfMediaRequest(request) {
		if(isWhitelisted(request) || /^main_frame/i.test(request.type))
			return {};
		var bm = getBlockedMediaForDomain(request);
		var res = false;
		var t='';
		var bFunc = function(t) {
			return console.log("Canceled request upon content check ("+t+")",request.url,res),cancelOrRedirect(request,t,res);
		}
		if(bm.disabled)	return {};
		if(bm.images && (res=isImage(request)))				return bFunc('Image');
		if(bm.videos && (res=isVideo(request)))				return bFunc('Video');
		if(bm.css && (res=isCSS(request)))					return bFunc('CSS');
		if(bm.js && (res=isJS(request)))					return bFunc('JS');
		if(bm.fonts && (res=isFont(request)))				return bFunc('Font');
		if(bm.social_media && (res=isSocialMedia(request)))	return bFunc('Social Media');
		if(bm.google_maps && (res=isGoogleMaps(request)))	return bFunc('GMaps');
		if(bm.marketing && (res=isMarketing(request)))		return bFunc('Marketing');
		return {};
	}
	// Function to toggle disabled on/off
	function toggleMediaType(mediaType,domain)	{
		if(domain) {
			blockedMediaTypes.byDomain[domain] = blockedMediaTypes.byDomain[domain] || {};
			if(mediaType == 'disabled' && blockedMediaTypes.byDomain[domain].disabled) {
				delete blockedMediaTypes.byDomain[domain];
			} else {
				blockedMediaTypes.byDomain[domain][mediaType] = blockedMediaTypes.byDomain[domain][mediaType]==1 ? 0 : 1;
			}
			
		} else {
			blockedMediaTypes[mediaType] = blockedMediaTypes[mediaType]==1 ? 0 : 1;
		}
		init();
		save();
	}
	function toggleDomainLevelSettings(domain) {
		if(blockedMediaTypes.byDomain[domain]) {
			if(blockedMediaTypes.byDomain[domain] && blockedMediaTypes.byDomain[domain].disabled)
				blockedMediaTypes.byDomain[domain] = {disabled:1};
			else
				delete blockedMediaTypes.byDomain[domain];
		} else {
			blockedMediaTypes.byDomain[domain] = {};
			for(var i in blockedMediaTypes) {
				if(typeof blockedMediaTypes[i] !== 'number')
					continue;
				blockedMediaTypes.byDomain[domain][i] = blockedMediaTypes[i];
			}
		}
		init();
		save();
	}
	function load(finalCallback) {
		var cb = function() {
			for(var i in defaultBlockedMediaTypes) {
				if(!blockedMediaTypes.hasOwnProperty(i))
					blockedMediaTypes[i] = defaultBlockedMediaTypes[i];
			}
		}
		if(blockedMediaTypes)
			return cb();
		try {
			tBrowser.storage.local.get('blockedMediaTypes', function(item) {
				blockedMediaTypes = item.blockedMediaTypes || defaultBlockedMediaTypes;
				return cb();
			});
		} catch(e) {
			return blockedMediaTypes = defaultBlockedMediaTypes, cb();
		}
	}
	function save() {
		try {
			tBrowser.storage.local.set({'blockedMediaTypes': blockedMediaTypes}, function() {
				// Notify that we saved.
				console.log("Settings updated",blockedMediaTypes);
			});
		} catch(e) {
			console.log("Failed to save settings");
		}
	}
	function getBlockedSoFar(tabId) {
		return blocks_by_tab[tabId] ? blocks_by_tab[tabId]['blocked_urls'] : [];
	}
	function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")
	}
	init();
	tBrowser.webRequest.onBeforeRequest.addListener(dropIfMediaRequest,{urls:["<all_urls>"]},["blocking"]);
	tBrowser.webRequest.onBeforeSendHeaders.addListener(dropIfMediaRequest,{urls:["<all_urls>"]},["blocking","requestHeaders"]);
	tBrowser.webRequest.onHeadersReceived.addListener(dropIfMediaRequest,{urls:["<all_urls>"]},["blocking","responseHeaders"]);
	tBrowser.webRequest.onHeadersReceived.addListener(addCSPHeader,{urls:["<all_urls>"]},["blocking","responseHeaders"]);
	if(tBrowser.webRequest.filterResponseData)	// Can only edit response data if this is present
		tBrowser.webRequest.onHeadersReceived.addListener(filterOutJavascript,{urls:["<all_urls>"]},["blocking","responseHeaders"]);
	//tBrowser.webRequest.onHeadersReceived.addListener(cacheIfApplicable,{urls:["<all_urls>"]},["blocking", "responseHeaders"]);
	// Function to provide info on what is blocked.
	tBrowser.runtime.onMessage.addListener(function(request, sender, sendResponse) {
		switch(request.msg) {
			case 'toggleDomainLevelSettings':
				return toggleDomainLevelSettings(request.domain),sendResponse(blockedMediaTypes),true
			case 'getBlockedMediaTypes':
				return sendResponse(blockedMediaTypes), true;
			case 'getBlockedSoFar':
				console.log(request);
				if(!request.tab || !request.tab.id) return true;
				return sendResponse(request.tab && request.tab.id ? getBlockedSoFar(request.tab.id) : []), true;
			case 'toggleMediaType':
				return toggleMediaType(request.mediaType,request.domain),sendResponse(blockedMediaTypes), true;
		}
	});
})();

