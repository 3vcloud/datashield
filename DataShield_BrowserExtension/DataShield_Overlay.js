(function() {
	window.DataShield = window.DataShield || {
		debug:1,
		overlayHTML:'TODO: Display injected HTML for DataShield',
		init:function() {
			if(this.initted)	return this;
			this.initted=1;
			var _this=this;
			
			// Set default XMLHttpRequest timeout to 10 secs
			this.XMLHttpRequest_Open = window.XMLHttpRequest.prototype.open;
			window.XMLHttpRequest.prototype.open = function() {
				var x = _this.XMLHttpRequest_Open.apply(this, arguments);
				x.timeout = 10000;
				_this.log("new XMLHttpRequest",x);
				return x;
			}
			this.debug && this.log("Set default XMLHttpRequest timeout to 10 secs");
			
			window.addEventListener('load',function() { _this.onDocumentReady.apply(_this,arguments); });
			if(window.readyState === 'complete')
				this.onDocumentReady();
			return this;
		},
		onDocumentReady:function() {
			if(this.documentReadied)
				return this;
			this.documentReadied=1;
			// Inject on-screen HTML.
			var d = document.createElement('div');
			d.id='DataShield_Container';
			d.className = 'DataShield_Container';
			d.style.display = 'none';
			d.innerHTML = this.overlayHTML;
			document.body.appendChild(d);
			this.debug && this.log("Injected on-screen HTML.");
			return this;
		},
		error:function() {
			var args = Array.prototype.slice.call(arguments);
			args.unshift('DataShield:');
			return console.error.apply(console,args);
		},
		log:function() {
			var args = Array.prototype.slice.call(arguments);
			args.unshift('DataShield:');
			return console.log.apply(console,args);
		},
		injectCSS:function() {
			if(this.cssInjected)	return this;
			this.cssInjected=1;
			
		}
	}
	return window.DataShield.init();
})();